package br.ifpe.web4.prac03;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Practice03Application {

	public static void main(String[] args) {
		SpringApplication.run(Practice03Application.class, args);
	}

}
