package br.ifpe.web4.prac03.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import br.ifpe.web4.prac03.models.Candidato;
import br.ifpe.web4.prac03.repositories.CandidatoRepository;

@Controller
public class CandidatoController {
	@Autowired
	private CandidatoRepository candidatoRepository;
	
	@GetMapping("/")
	public String home() {
		return "index";
	}
	
	@GetMapping("/cadastro")
	public String cadastro(Candidato candidato) {
		return "cadastro";
	}
	
	@PostMapping("/salvar")
	public String salvar(Candidato candidato) {
		this.candidatoRepository.save(candidato);
		return "redirect:/listar";
	}
	
	@GetMapping("/listar")
	public String listar(Model model) {
		model.addAttribute("candidatos", this.candidatoRepository.findAll());
		return "listar-pesquisar";
	}
	
	@PostMapping("/pesquisar")
	public String pesquisar(String nome, Model model) {
		
		List<Candidato> resultado = new ArrayList<>();
		for(Candidato c : this.candidatoRepository.findAll()) {
			if(c.getNome().toLowerCase().contains(nome.toLowerCase())) {
				resultado.add(c);
			}
		}
		model.addAttribute("candidatos", resultado);
		
		return "listar-pesquisar";
	}
	
	@GetMapping("/editar")
	public String editarCliente(Integer id, Model model) {
		Candidato candidato = this.candidatoRepository.getById(id);
		model.addAttribute("candidato", candidato);
		return "cadastro";
	}
	
	@GetMapping("/remover")
	public String remover(Integer id) {
		this.candidatoRepository.deleteById(id);
		return "redirect:/listar";

	}
}
