package br.ifpe.web4.prac03.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.ifpe.web4.prac03.models.Candidato;

public interface CandidatoRepository extends JpaRepository<Candidato, Integer> {

}
