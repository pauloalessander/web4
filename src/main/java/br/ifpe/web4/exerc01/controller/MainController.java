package br.ifpe.web4.exerc01.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {
	
	@GetMapping("/")
	public String home() {
		return "home";
	}

	@GetMapping("/ind")
	public String index(Model model) {
		model.addAttribute("mensagem", "Povo de Informática");
		return "index";
	}

	@GetMapping("/ind2")
	public String index2(Model model) {
		model.addAttribute("mensagem", "Les Gens de l'Informatique");
		return "index";
	}

	@GetMapping("/ind3")
	public String index3(Model model) {
		model.addAttribute("mensagem", "Leute der Informatik");
		return "index";
	}

	@GetMapping("/ind4")
	public String index4(Model model) {
		model.addAttribute("mensagem", "Gente de Informatica");
		return "index";
	}

	@GetMapping("/ind5")
	public String index5(Model model) {
		model.addAttribute("mensagem", "Gente di Informatica");
		return "index";
	}

	@GetMapping("/ind6")
	public String index6(Model model) {
		model.addAttribute("mensagem", "Informatiikan Ihmiset");
		return "index";
	}

	@GetMapping("/ind7")
	public String index7(Model model) {
		model.addAttribute("mensagem", "People of Informatics");
		return "index";
	}
}
